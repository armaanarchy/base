# Introducing Arma Anarchy

Arma Anarchy is a mod built for Arma Reforger and ultimately Arma 4. It's centered in a world with no central government which is ruled by dueling factions. You're a member of one of those factions. Loot, Steal, Pillage, Divide, Conquer; Anything to get on-top.

[Visit our Wiki for the full scoop.](https://armaanarchy.gitlab.io/base/#/)

# Developer Introduction

Hey! You want to help out, that's great! Everyone can contribute. Create a branch or fork this repository, make your changes, and submit them back as a merge request to the main branch of this project. Make sure there's an issue attached to it describing the changes to be done.

If you don't know what to work on, go ahead and look over our issues. We've tried to make them as organized as possible. Issues that are good for newcomers are marked as "Newcomer".

## Contributing
TODO

## Installation
TODO

## Support
TODO

## Roadmap
See WIKI

## License
GPL3 License.

## Project status
Currently Active Development
